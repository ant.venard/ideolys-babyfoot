--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: games; Type: TABLE; Schema: public; Owner: antoine
--

CREATE TABLE public.games (
    id integer NOT NULL,
    name character varying(255),
    created timestamp without time zone DEFAULT now() NOT NULL,
    played boolean DEFAULT false NOT NULL
);


ALTER TABLE public.games OWNER TO antoine;

--
-- Name: games_id_seq; Type: SEQUENCE; Schema: public; Owner: antoine
--

CREATE SEQUENCE public.games_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.games_id_seq OWNER TO antoine;

--
-- Name: games_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antoine
--

ALTER SEQUENCE public.games_id_seq OWNED BY public.games.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: antoine
--

CREATE TABLE public.messages (
    id integer NOT NULL,
    message character varying(255),
    created timestamp without time zone DEFAULT now() NOT NULL,
    user_message character varying(255)
);


ALTER TABLE public.messages OWNER TO antoine;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: antoine
--

CREATE SEQUENCE public.message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.message_id_seq OWNER TO antoine;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antoine
--

ALTER SEQUENCE public.message_id_seq OWNED BY public.messages.id;


--
-- Name: games id; Type: DEFAULT; Schema: public; Owner: antoine
--

ALTER TABLE ONLY public.games ALTER COLUMN id SET DEFAULT nextval('public.games_id_seq'::regclass);


--
-- Name: messages id; Type: DEFAULT; Schema: public; Owner: antoine
--

ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.message_id_seq'::regclass);


--
-- Data for Name: games; Type: TABLE DATA; Schema: public; Owner: antoine
--

COPY public.games (id, name, created, played) FROM stdin;
127	Antoine VS Jerome	2019-04-17 14:22:29.288448	f
128	George VS Antoine	2019-04-17 14:22:32	f
129	Jérome VS George	2019-04-17 14:22:33	f
\.


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: antoine
--

COPY public.messages (id, message, created, user_message) FROM stdin;
42	Bonjour	2019-04-17 14:22:51.76691	Antoine
62	Coucou	2019-04-17 16:17:44.339938	Antoine
\.


--
-- Name: games_id_seq; Type: SEQUENCE SET; Schema: public; Owner: antoine
--

SELECT pg_catalog.setval('public.games_id_seq', 130, true);


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: antoine
--

SELECT pg_catalog.setval('public.message_id_seq', 66, true);


--
-- Name: games games_pkey; Type: CONSTRAINT; Schema: public; Owner: antoine
--

ALTER TABLE ONLY public.games
    ADD CONSTRAINT games_pkey PRIMARY KEY (id);


--
-- Name: messages message_pkey; Type: CONSTRAINT; Schema: public; Owner: antoine
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

