require('dotenv').config();
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const database = require('./js/queries.js');

app.use(express.static(`${__dirname}/js`));
app.use(express.static(`${__dirname}/styles`));

app.get('/', (req, res) => {
	res.sendFile(`${__dirname}/index.html`);
});

io.on('connection', (socket) => {
	socket.on('addGame', name => {
		database.addGame(name)
			.then(() => {
				displayList();
			})
			.catch(() => {
				socket.emit('sqlError', 'Impossible d\'ajouter la partie');
			});
	});

	socket.on('deleteGame', id => {
		database.deleteGame(id)
			.then(() => {
				displayList();
			})
			.catch(() => {
				socket.emit('sqlError', 'Impossible de supprimer la partie');
			});
	});

	socket.on('setGameStatus', id => {
		database.setGameStatus(id)
			.then(() => {
				displayList();
			})
			.catch(() => {
				socket.emit('sqlError', 'Impossible de modifier la partie');
			});
	});

	const displayList = () => {
		database.getGames()
			.then((results) => {
				io.emit('displayNew', results.rows);
			})
			.catch(() => {
				socket.emit('sqlError', 'Impossible de récupérer la liste');
			});
	};

	displayList();
});

http.listen(process.env.PORT, process.env.IP_HOST, () => {});
