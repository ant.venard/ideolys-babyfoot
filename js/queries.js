const Pool = require('pg').Pool;
const pool = new Pool({
	user: process.env.DB_USER,
	host: process.env.DB_HOST,
	database: process.env.DB_NAME,
	password: process.env.DB_PASS,
	port: 5432,
});

exports.addGame = name => {
	return new Promise((resolve, reject) => {
		const sql = `INSERT INTO games (name) VALUES ('${name}');`;
		pool.query(sql, (error, results) => {
			if (error) {
				reject(error.message);
				return;
			}

			resolve(results);
		});
	});
};

exports.deleteGame = id => {
	return new Promise((resolve, reject) => {
		const sql = `DELETE FROM games WHERE id = ${id};`;
		pool.query(sql, (error, results) => {
			if (error) {
				reject(error.message);
				return;
			}

			resolve(results);
		});
	});
};

exports.setGameStatus = id => {
	return new Promise((resolve, reject) => {
		const sql = `UPDATE games SET played = NOT played WHERE id = ${id};`;
		pool.query(sql, (error, results) => {
			if (error) {
				reject(error.message);
				return;
			}

			resolve(results);
		});
	});
};

exports.getGames = () => {
	return new Promise((resolve, reject) => {
		const sql = 'SELECT * FROM games ORDER BY created ASC;';
		pool.query(sql, (error, results) => {
			if (error) {
				reject(error.message);
				return;
			}

			resolve(results);
		});
	});
};