const socket = io();

const newGame = () => {
	const value = document.getElementById('gameText').value;
	if (!value) {
		showError('Veuillez saisir un match');
		return;
	}

	document.getElementById('gameText').value = '';
	document.getElementById('modal').checked = false; // Hide modal
	socket.emit('addGame', value);
	return;
};

const deleteGame = id => {
	if (!id) {
		showError('Impossible de supprimer ce match');
		return;
	}

	socket.emit('deleteGame', id);
	return;
};

const setGameStatus = id => {
	if (!id) {
		showError('Impossible de modifier le statut du match');
		return;
	}
	
	document.getElementById('played' + id).innerHTML = 'Rejouer';
	socket.emit('setGameStatus', id);
	return;
};

// On event displayNew, dislay the game list
socket.on('displayNew', games => {
	document.getElementById('gameList').innerHTML = ''; // Empty list
	if (games.length === 0) {
		document.getElementById('counter').innerHTML = 'Aucun match à jouer';
		return;
	}	

	const count = games.filter(game => !game.played).length;
	document.getElementById('counter').innerHTML = 'Match restant: ' + count;

	games.forEach(game => {
		addGameToList(game);
	});
});

socket.on('sqlError', error => {
	showError(error);
});

const showError = msg => {
	const toast = document.getElementById('toast');
	document.getElementById('errorText').innerHTML = msg;
	toast.className = 'show';
	setTimeout(() => { toast.className = toast.className.replace('show', ''); }, 3000);
};

const addGameToList = game => {
	if (!game.id || !game.name) {
		return;
	}

	const text = (game.played) ? 'Rejouer' : 'Terminer';
	const id = game.id;
	const template = 
	'<li id="'+ id  + '" class="flex-container">' +
		'<p id="label' + id + '">' + game.name + '</p>' +
		'<div class="flex-item">' +
			'<a id="played' + id + '" class="played-button" onClick="setGameStatus(' + id + ')">' + text + '</a>' + 
			'<a class="delete-button" onClick="deleteGame(' + id + ')">X</a>' + 
		'</div>' +
	'</li>';
	
	document.getElementById('gameList').insertAdjacentHTML('beforeend', template);
	if (game.played) {
		document.getElementById('label' + game.id).classList.add('played');
	}
};
