# Ideolys Babyfoot

Babyfoot manager project for Ideolys.

## Getting Started

### Prerequisites

You must have created ```babyfoot``` database.
(use: ```babyfoot_dump_17042019.sql```)

### Installing

Use command ```yarn``` to install packages.

Create ```.env``` file.

Set the ```.env``` file:
   
    DB_HOST='localhost'
    
    DB_NAME='YOUR_DATABASE_NAME_POSTGRESQL'
    
    DB_USER='YOUR_USER_NAME_POSTGRESQL'
    
    DB_PASS='YOUR_PASSWORD_POSTGRESQL'
    
    IP_HOST='0.0.0.0'
    
    PORT='YOUR_PORT'
    

Use command ```yarn start``` to launch project.

Go to ```http://127.0.0.1:[YOUR_PORT]```

To see this from another device, you need to get the ip of the local host.

Go to  ```http:[IP_LOCAL_HOST]:[YOUR_PORT]```

## Versioning

You can find this project repository here: https://gitlab.com/ant.venard/ideolys-babyfoot

## Authors

* **Antoine VENARD**